import shutil, os
import glob
import subprocess
import sys

blenderExecutable = 'blender'
tmp = './.tmp'
testname = ''

output_path = os.path.realpath(tmp)
exact_test = False
print(output_path)
# allow override of blender executable (important for CI!)
if len(sys.argv) > 1:
    blenderExecutable = sys.argv[1]
if len(sys.argv) > 2:
    testname = sys.argv[2]
files = glob.glob('./tests/%s**/*.test.blend' % testname)
if len(sys.argv) > 3:
    exact_test = True
    files = glob.glob('./tests/**/%s.blend' % testname)

# iterate over each *.test.blend file in the "tests" directory
# and open up blender with the .test.blend file and the corresponding .test.py python script
for file in files:
    if os.path.isdir( output_path):
        shutil.rmtree(output_path)
    if not os.path.isdir( output_path):
        os.makedirs( output_path, exist_ok = True)

    test = os.path.split(os.path.split(file)[0])[1]

    print("-----------------")
    print("Executing test: %s" % test)
    print("-----------------")
    try:
        subprocess.check_call([blenderExecutable, '--python-exit-code', '2', '--addons', 'render_corona', '--factory-startup', '-noaudio', '-E', 'CORONA', '-b', file, '--python', file.replace('.blend', '.py')])
    except:
        print("-----------------")
        print("Some tests FAILED: %s" % test)
        print("-----------------")
        sys.exit(1)

print("----------------")
print("All tests passed")
print("----------------")
import unittest
import bpy
import render_corona
from render_corona.tests import *

class TestBasic(unittest.TestCase):
    def test_export_scene(self):
        bpy.ops.corona.export_scene()
        self.assertTrue(CompareXml('tests/02 uvmaps/Scene.scn', '.tmp/Scene.scn'))

    def test_render(self):
        bpy.ops.render.render()
        # The image is as expected
        self.assertTrue(CompareImg('tests/02 uvmaps/Scene1.png', '.tmp/render/Scene1.png', 5))

        # The cube is as expected
        self.assertTrue(CompareFiles('tests/02 uvmaps/meshes/Cube.cgeo', '.tmp/meshes/Cube.cgeo'))

        # The cube.001 is as expected
        self.assertTrue(CompareFiles('tests/02 uvmaps/meshes/Cube.001.cgeo', '.tmp/meshes/Cube.001.cgeo'))

RunTests(TestBasic)
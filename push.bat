@Echo off


set /p oldversion=<version.txt

echo Tagging: %oldversion%

git tag v%oldversion%

git push --tags origin HEAD

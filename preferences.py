import bpy
from bpy.props import StringProperty, BoolProperty
from .util import setVerbose, setDebug

# updater ops import, all setup in this file
from . import addon_updater_ops

def dummyupdate(self=None, context=None):
  try:
    setDebug(bpy.context.user_preferences.addons[__package__].preferences.corona_debug)
    setVerbose(bpy.context.user_preferences.addons[__package__].preferences.corona_verbose)
    setMultiThreaded(bpy.context.user_preferences.addons[__package__].preferences.corona_multithreaded)
  except:
    pass

#------------------------------------
# User preferences UI
#------------------------------------
class CoronaPreferencesPanel( bpy.types.AddonPreferences):
    bl_idname = __package__
    corona_path = StringProperty( name = "Corona Path",
                                       description = "Path to Corona executable directory",
                                       subtype = 'DIR_PATH',
                                       default = "")
    corona_exe = StringProperty( name = "Corona Executable",
                                       description = "Corona executable filename",
                                       subtype = 'NONE',
                                       default = "CoronaStandalone_Release.exe")

    corona_debug = BoolProperty( name = "Corona Debug",
                                description = "Enable debug output in the system console to help with errors",
                                default = False,
                                update = dummyupdate)

    corona_verbose = BoolProperty( name = "Corona Verbose",
                                description = "Enable pretty printing xml (slower) to help with errors",
                                default = False,
                                update = dummyupdate)

    corona_multithreaded = BoolProperty( name = "Corona Multi Threaded",
                                description = "Speed up exporting by using all your CPU cores",
                                default = True,
                                update = dummyupdate)

    convert_cycles = BoolProperty( name = "Convert Cycles nodes to Corona nodes",
                                description = "Automatically try to convert Cycle node layout to a Corona node layout",
                                default = True,
                                update = dummyupdate)

    # addon updater preferences

    auto_check_update = bpy.props.BoolProperty(
      name = "Auto-check for Update",
      description = "If enabled, auto-check for updates using an interval",
      default = False,
      )
    
    updater_intrval_months = bpy.props.IntProperty(
      name='Months',
      description = "Number of months between checking for updates",
      default=0,
      min=0
      )
    updater_intrval_days = bpy.props.IntProperty(
      name='Days',
      description = "Number of days between checking for updates",
      default=7,
      min=0,
      )
    updater_intrval_hours = bpy.props.IntProperty(
      name='Hours',
      description = "Number of hours between checking for updates",
      default=0,
      min=0,
      max=23
      )
    updater_intrval_minutes = bpy.props.IntProperty(
      name='Minutes',
      description = "Number of minutes between checking for updates",
      default=0,
      min=0,
      max=59
      )

    def draw(self, context):
      self.layout.prop( self, "corona_path")
      self.layout.prop( self, "corona_exe")
      self.layout.prop( self, "corona_debug")
      self.layout.prop( self, "corona_verbose")
      self.layout.prop( self, "corona_multithreaded")
      self.layout.prop( self, "convert_cycles")
      
      # updater draw function
      addon_updater_ops.update_settings_ui(self,context)


class DemoUpdaterPanel(bpy.types.Panel):
  """Panel to demo popup notice and ignoring functionality"""
  bl_label = "Corona Updater"
  bl_idname = "OBJECT_PT_corona_updater"
  bl_space_type = 'VIEW_3D'
  bl_region_type = 'TOOLS'
  bl_context = "objectmode"
  bl_category = "Tools"

  def draw(self, context):
    layout = self.layout

    # Call to check for update in background
    # note: built-in checks ensure it runs at most once
    # and will run in the background thread, not blocking
    # or hanging blender
    # Internal also checks to see if auto-check enabeld
    # and if the time interval has passed
    # addon_updater_ops.check_for_update_background(context)


    # could also use your own custom drawing
    # based on shared variables
    if addon_updater_ops.updater.async_checking == True:
      layout.label("Corona Render checking for update", icon="INFO")
    elif addon_updater_ops.updater.update_ready != True:
      layout.label("Corona Render up to date", icon="INFO")

    # call built-in function with draw code/checks
    addon_updater_ops.update_notice_box_ui(self, context)



def register():

    bpy.utils.register_class( DemoUpdaterPanel)
    bpy.utils.register_class( CoronaPreferencesPanel)
    addon_updater_ops.check_for_update_background()
    dummyupdate()

def unregister():
    bpy.utils.unregister_class( DemoUpdaterPanel)
    bpy.utils.unregister_class( CoronaPreferencesPanel)

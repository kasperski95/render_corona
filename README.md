### Documentation ###
[Release Notes For Exporter](https://bitbucket.org/coronablender/render_corona/wiki/Release%20Notes)

[Corona Standalone Documentation](https://corona-renderer.com/wiki/standalone)

### Forums ###

https://corona-renderer.com/forum/index.php/board,28.0.html
### MacOS ###

Use exporter [v2.4.12](https://bitbucket.org/coronablender/render_corona/get/v2.4.12.zip)

Use these settings in the render corona plugin settings page:

Corona Path: /Applications/Corona/Corona.app/Contents/MacOS/ 

Corona Executable: Corona1.5.Standalone

### Version 3.0.0+ requires a new corona release! Blender 2.77+ ###
Exporter: [latest version](https://bitbucket.org/coronablender/render_corona/get/master.zip)

Corona [Build 2017-01-17](https://www.dropbox.com/sh/kgzu0cqy903ygmb/AABF4EHPQ6ihxVIZRU1VtBPza/Standalone/2017-01-17%20-%20release%20and%20assert%2C%20fullspeed%20and%20legacy/fullspeed?dl=1)
Corona for older CPU's [Legacy 2017-01-17](https://www.dropbox.com/sh/kgzu0cqy903ygmb/AADxhAVXP4TjcORPqZ3cYSKBa/Standalone/2017-01-17%20-%20release%20and%20assert%2C%20fullspeed%20and%20legacy/legacy?dl=1)

### Versions up to 2.4.13 Blender 2.77+ ###
Exporter: [v2.4.13](https://bitbucket.org/coronablender/render_corona/get/v2.4.13.zip)

Corona: [Build 2016-10-24](https://www.dropbox.com/sh/kgzu0cqy903ygmb/AAA7gpdtb22Veqi4tzhJPjUka/Standalone/2016-10-24?dl=0)
### Other versions ###

Newer versions of Corona Standalone might be published here: [Standalone](https://www.dropbox.com/sh/kgzu0cqy903ygmb/AAA1CTqR1Uannm_D0J9JSo1Ga/Standalone?dl=0) or follow the daily build links from here: [Corona Doco](https://coronarenderer.freshdesk.com/support/solutions/articles/5000570015-daily-builds)

### Installation ###
Put into your blender/scripts/addons directory and configure using the installation steps from here: [Installation Instructions](https://corona-renderer.com/wiki/blender2corona/installation)


Due to popular demand if you want to gift a beer or two you can do so here: paypal.me/DeveloperGlen and if that doesn't work and you still want to this might work 
[![Become a Patron!](https://img.shields.io/badge/patreon-donate-orange.svg)](https://www.patreon.com/bePatron?u=6437268)

### Development ###
The easiest way to develop is to fork a copy and checkout.  Then create a branch and commit/push to that.  When you are ready then submit a pull request through bitbucket.

#### Testing ####
There is a way to test some of the functionality right now.  You need to have python installed somewhere (even the python included with blender is good enough)

Set the environment variable `CORONA=<path to corona standalone folder here>`

Then run `python tests.py "<path to blender.exe here>"`

This takes a little while to run and will test some of the basic functionality.

Additional tests can be added by creating a new subdirectory in `tests/<your dir>`
Include a `<yourtest>.test.py` and a `<yourtest>.blend`

See `tests/01 basic` for an example. Note that tests can only use the libraries that are included with blender.  There are helper functions for comparing plain text files, xml files and image files.
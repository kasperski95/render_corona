import bpy
import nodeitems_utils
import subprocess, os, time, sys
import threading
from extensions_framework   import util as efutil
from .util                  import realpath, resolution, plugin_path, get_instance_materials, get_version
#, get_bitbucket_version
from .util                  import sep, CrnUpdate, CrnProgress, CrnError, CrnInfo, debug, name_compat, resolve_export_path, findCoronaOutputNode
from .outputs               import export_preview
from .properties.nodes      import CoronaNode
from .cycles                import migrate_cycles_nodes
import traceback
import json
from bpy.app.handlers   import persistent
#------------------
# Generic Utilities
#------------------

@persistent
def scene_loaded(scene):
    test_renderer_changed()
    renderer = bpy.context.scene.render.engine
    if renderer == "CORONA":
        try:
            migrate_old_node_tree()
            # migrate_corona_use_nodes()
            migrate_cycles_nodes()
        except:
            print("Unexpected error during scene change:", sys.exc_info()[0])
            traceback.print_exc()

def migrate_corona_use_nodes():
    materials = bpy.data.materials
    debug("Migrating corona.uses_nodes", len(materials))
    for material in materials:
        debug(" ", material)
        crn_mat = material.corona
        if not crn_mat.use_nodes:
            if findCoronaOutputNode(material):
                crn_mat.use_nodes = True

def migrate_old_node_tree():
    # For each material
    # Get corona_material
    # Check if it has a node_tree
    # Load from bpy.data.node_groups
    # migrate all nodes and links to material.node_tree
    # remove corona_material.node_tree
    materials = bpy.data.materials
    debug("Migrating materials", len(materials))
    for material in materials:
        debug(" ", material)
        crn_mat = material.corona
        node_tree = None
        if hasattr(crn_mat, "node_tree") and \
            crn_mat.node_tree != '' and \
            bpy.data.node_groups.find(crn_mat.node_tree) != -1:
            node_tree = bpy.data.node_groups[crn_mat.node_tree]
            # reset it here so we don't get into a loop
            crn_mat.node_tree = ''
        debug("  ", node_tree)
        material.use_nodes = True
        if node_tree:
            crn_mat.use_nodes = True
            mtree = material.node_tree
            debug("     migration tree", mtree)
            if mtree:
                mnodes = mtree.nodes
                mnewNode = mnodes.new
                mnewLink = mtree.links.new
                nodes = node_tree.nodes
                links = node_tree.links
                debug("     Migrating nodes ", len(nodes), len(links))
                for node in nodes:
                    if isinstance(node, CoronaNode):
                        mnode = mnewNode(node.bl_idname)
                        debug("       New node", mnode, dict(node))
                        for key, value in dict(node).items():
                            debug("           ", key, value)
                            mnode[key] = value
                        mnode.name = node.name
                        mnode.height = node.height
                        mnode.width = node.width
                        mnode.location = node.location
                        for input in node.inputs:
                            if mnode.inputs.find(input.name) != -1:
                                minput = mnode.inputs[input.name]
                                for key, value in dict(input).items():
                                    debug("           ", input.name, key, value)
                                    mkey = key
                                    if mkey == "socket_val":
                                        mkey = "default_value"
                                    minput[mkey] = value
                for link in links:
                    try:
                        if isinstance(link.from_node, CoronaNode):
                            debug("       Old link", link.from_node, link.to_node)
                            mfrom_node = mnodes[link.from_node.name]
                            if link.from_socket.is_output:
                                mfrom_socket = mfrom_node.outputs[link.from_socket.identifier]
                            else:
                                mfrom_socket = mfrom_node.inputs[link.from_socket.identifier]
                            mto_node = mnodes[link.to_node.name]
                            if link.to_socket.is_output:
                                mto_socket = mto_node.outputs[link.to_socket.identifier]
                            else:
                                mto_socket = mto_node.inputs[link.to_socket.identifier]
                            mlink = mnewLink(mfrom_socket, mto_socket)
                            debug("       New link", mlink)
                    except:
                        print("Unexpected error:", sys.exc_info()[0])
                        raise



_old_renderer = '_____'
unregistered_categories = {}
COMMON_NODE_CATEGORIES = ['COMPOSITING', 'TEXTURE', 'CORONA_NODES']

@persistent
def test_renderer_changed(dummy = None):
    global _old_renderer
    renderer = bpy.context.scene.render.engine
    if _old_renderer != renderer:
        debug("Renderer change", _old_renderer, '->', renderer)
        if renderer == "CORONA":
            try:
                hide_other_categories()
            except:
                print("Unexpected error during render change:", sys.exc_info()[0])
        else:
            show_other_categories()
        _old_renderer = renderer

def hide_other_categories():
    global unregistered_categories, COMMON_NODE_CATEGORIES
    unregistered_categories = {}
    for ident, node_item in nodeitems_utils._node_categories.items():
        if ident not in COMMON_NODE_CATEGORIES:
            debug("Hiding", ident)
            unregistered_categories[ident] = node_item
    for ident, node_item in unregistered_categories.items():
        nodeitems_utils.unregister_node_categories(ident)

def show_other_categories():
    global unregistered_categories
    for ident, node_item in unregistered_categories.items():
        nodeitems_utils.register_node_categories(ident, node_item[0])
    unregistered_categories = {}

def update_start( engine, data, scene):
    if engine.is_preview:
        update_preview( engine, data, scene)
    else:
        update_scene( engine, data, scene)

def render_start( engine, scene):
    if engine.is_preview:
        render_preview( engine, scene)
    else:
        if engine.animation:
            # Step through frames to render animation
            # animation can only be True if rendering with command line
            frame_start = scene.frame_start
            frame_current = frame_start
            scene.frame_set( frame_start)
            frame_end = scene.frame_end
            step = scene.frame_step
            while frame_current <= frame_end:
                render_scene( engine, scene)
                frame_current += frame_step
                scene.frame_set( frame_current)
        else:
            render_scene( engine, scene)

def render_init( engine):
    pass

def update_preview( engine, data, scene):
    pass

def update_scene( engine, data, scene):
    pass

from pprint import pprint

def gatherOutput(proc, startup, until, showOutput = True):
    buf = ''
    while proc.poll() == None:
        data = proc.stdout.read(1)
        buf += data
        if showOutput:
            sys.stdout.write(data)
            sys.stdout.flush()
        if buf == until or data == '\n':
            if buf == until:
                return True
            else:
                buf = ''
    return False

def update_result(engine, width, height, output_file):
    # debug("Reading result %s %d %d" % (output_file, width, height))
    try:
        result = engine.begin_result( 0, 0, width, height)
        lay = result.layers[0]
        lay.load_from_file( output_file)
        engine.end_result( result, cancel=False, do_merge_results=True)
        return True
    except:
        traceback.print_exc()
        engine.end_result( None, cancel=True, do_merge_results=False)
        pass
    return False

corona_proc = None
uid = 0
def kill_preview():
    global corona_proc
    # debug("Preview process", corona_proc)
    if corona_proc and corona_proc.poll() == None:
        corona_proc.terminate()
#----------------------------------
# Render the material preview
#---------------------------------
def render_preview( engine, scene):
    global corona_proc, uid

    if bpy.context.user_preferences.addons[__package__].preferences.corona_path == '':
        engine.report( {'INFO'}, 'Error: The binary path is unspecified! Check Corona addon user preferences.')
        return
    # Iterate through the preview scene, finding objects with materials attached
    objects_materials = {}
    (width, height) = resolution( scene)

    for object in [ob for ob in scene.objects if ob.is_visible( scene) and not ob.hide_render]:
        for matname in get_instance_materials( object, scene.corona):
            try:
                mat = bpy.data.materials[matname]
                if mat is not None:
                    if not object.name in objects_materials.keys(): objects_materials[object] = []
                    objects_materials[object].append( mat)
            except:
                pass
    # find objects that are likely to be the preview objects
    preview_objects = [o for o in objects_materials.keys() if o.name.startswith( 'preview')]
    if len( preview_objects) < 1:
        return

    # find the materials attached to the likely preview object
    likely_materials = objects_materials[preview_objects[0]]
    if len( likely_materials) < 1:
        return

    corona_path = realpath( bpy.context.user_preferences.addons[__package__].preferences.corona_path)
    corona_path = os.path.join( corona_path, bpy.context.user_preferences.addons[__package__].preferences.corona_exe)

    uid += 1
    if uid > 20:
        uid = 0
    tempdir = efutil.temp_directory()
    output_file = os.path.join( os.path.join( tempdir, "matpreview"), "matpreview%d.png" % uid)
    output_path = os.path.join( tempdir, "matpreview")
    if not os.path.isdir( output_path):
        os.makedirs( output_path, exist_ok = True)
    pm = likely_materials[0]

    preview_quality = pm.corona.preview_quality
    if preview_quality > 0.5 and width < 50:
        preview_quality = 0.5
    # debug('%.2f %s %d %d' % (preview_quality, output_file, width, height))
    exporter = export_preview( scene, output_file, pm, width, height, bpy.data.materials, bpy.data.textures)
    # debug('material', exporter)
    if exporter == None:
        CrnError( 'Error while exporting -- check the console for details.')
        return
    else:
        if not bpy.app.background:
            startup = False
            # Start the preview process if it isn't running
            if corona_proc == None or corona_proc.poll() != None:
                debug("Starting preview process")
                cmd = ( corona_path, '-mtlPreview')
                startup = True
                corona_proc = subprocess.Popen( cmd,stderr=subprocess.PIPE,stdout=subprocess.PIPE,stdin=subprocess.PIPE,shell=(os.name!='posix'),cwd=output_path,bufsize=1,universal_newlines=True)
                gatherOutput(corona_proc, startup, '\n')
                debug("Ready to render previews")
                startup = False

            # Preview at real low quality first
            # if pm.corona.preview_quality > 1 and width > 100:
            #     preview_quality = 0.1
            #     corona_proc.stdin.write('%.3f %s' % (preview_quality, exporter))
            #     corona_proc.stdin.flush()
            #     gatherOutput(corona_proc, startup, '.')
            #     update_result(engine, width, height, output_file)

            # This is the real preview render
            preview_quality = pm.corona.preview_quality
            if preview_quality > 1 and width < 50:
                preview_quality = 1
            corona_proc.stdin.write('%.3f %s' % (preview_quality, exporter))
            corona_proc.stdin.flush()
            gatherOutput(corona_proc, startup, '.', showOutput = False)

            if not update_result(engine, width, height, output_file):
                err_msg = 'Error: Could not load render result from %s.' % output_file
                CrnError( err_msg)

            # sys.stdout.write('\n')
            # sys.stdout.flush()
            try:
                os.remove(output_file)
            except:
                pass

#----------------------------------
# Render and export the scene
#---------------------------------
def render_scene( engine, scene):

    DELAY = 0.5

    if scene.corona.export_path == '':
        engine.report({'WARNING'}, "Export path is not specified!  Set export path in Render tab, under Corona Render panel.")
        return

    render_dir = 'render'
    bpy.ops.corona.export_scene()
    bpy.ops.corona.export_mat()
    if scene.corona.obj_export_bool == True:
        # bpy.context.window_manager['corona'] = None
        result = bpy.ops.corona.export()
        if result != {'FINISHED'}:

            engine.report({'WARNING'}, bpy.context.window_manager['corona']['msg'])
            # bpy.context.window_manager['corona'] = None
            return

    render_output = os.path.join( resolve_export_path(scene.corona), render_dir)
    width = scene.render.resolution_x
    height = scene.render.resolution_y

    # Make the render directory, if it doesn't exist
    if not os.path.isdir( render_output):
        os.makedirs( render_output, exist_ok = True)

    try:
        for item in os.listdir( render_output):
            if item == name_compat(scene.name) + '_' + str( scene.frame_current) + scene.corona.image_format:
                os.remove( render_output + item)
    except:
        pass

    # Set filename to render
    filename = name_compat(scene.name) + ".scn"

    filename = os.path.join( resolve_export_path(scene.corona), filename)
    imagename = os.path.join( render_output, ( name_compat(scene.name) + str( scene.frame_current)))

    # Start the Corona executable
    # Get the absolute path to the executable dir
    corona_path = realpath( bpy.context.user_preferences.addons[__package__].preferences.corona_path)
    corona_path = os.path.join( corona_path, bpy.context.user_preferences.addons[__package__].preferences.corona_exe)

    render_image = imagename + scene.corona.image_format
    flags = '-oA' if scene.corona.save_alpha else '-o'
    cmd = None
    if scene.corona.vfb_type == '0':
        cmd = ( corona_path, filename, flags, render_image, '-silent')
    else:
        cmd = ( corona_path, filename, flags, render_image)
    debug("Command", len(cmd), (',').join(cmd))
    CrnUpdate( "corona %s file %s image %s" % (corona_path, filename, render_image))
    CrnUpdate( "Rendering scene file {0}...".format( cmd[1].split( '\\')[-1]))
    CrnUpdate( "Launching Corona Renderer...")
    process = subprocess.Popen( cmd, cwd = render_output)

    # The rendered image name and path
    # render_image = imagename + scene.corona.image_format
    # Wait for the file to be created
    while not os.path.exists( render_image):
        if engine.test_break():
            debug('Trying to kill the process')
            try:
                process.kill()
            except:
                pass
            break

        if process.poll() != None:
            engine.update_stats( "", "Corona: Error")
            break

        time.sleep( DELAY)


    if os.path.exists( render_image):
        CrnUpdate( "Output image exists...")
        engine.update_stats( "", "Corona: Rendering")

        prev_size = -1

        def update_image():
            result = engine.begin_result( 0, 0, width, height)
            lay = result.layers[0]
            # possible the image wont load early on.
            try:
                lay.load_from_file( render_image)
            except:
                pass

            engine.end_result( result)

        # Update while rendering
        while True:
            # debug('Process', process.poll())
            if process.poll() != None:
                debug( "Updating Image")
                update_image()
                break

            #user exit
            if engine.test_break():
                debug('Trying to kill the process')
                try:
                    process.kill()
                except:
                    pass
                # break

            # check if the file updated
            new_size = os.path.getmtime( render_image)

            # debug( "Image Size:", new_size)
            if new_size != prev_size:
                update_image()
                prev_size = new_size

            time.sleep( DELAY)


# __STARTUP__ = True

#----------------------------------------------
# Render engine/settings
#----------------------------------------------

class RenderCorona( bpy.types.RenderEngine):
    """Corona render engine class"""
    bl_idname = "CORONA"
    bl_label = "Corona"
    bl_use_preview = True
    bl_use_shading_nodes = True
    bl_use_shading_nodes_custom = False


    render_lock = threading.Lock()
    preview_lock = threading.Lock()
    animation = False

    def __init__( self):
        render_init( self)

    # final rendering
    def update( self, data, scene):
        update_start( self, data, scene)

    def render( self, scene):


        # global __STARTUP__

        # if __STARTUP__:
        #     bitbucketVersion = get_bitbucket_version()
        #     version = get_version()

        #     if bitbucketVersion != version:
        #         self.report({'INFO'}, 'New Corona export update available')
        #     __STARTUP__ = False

        # debug("render")
        if self is None or scene is None:
            CrnError( 'Scene is missing! Please select a scene to render')
            return
        if bpy.context.user_preferences.addons[__package__].preferences.corona_path == '':
            CrnError( 'The binary path is unspecified! Check Corona addon user preferences.')
            return

        if self.is_preview: # preview and normal shouldn't clash
            # debug("Render before lock")
            with self.preview_lock:
                render_preview( self, scene)
            # debug("Render after lock")
        else:
            with self.render_lock:  # just render one thing at a time
                render_start( self, scene)



